# GimmePizza

Projeto teste de um delivery de pizzas da empresa [Frezze](https://www.frezze.com.br/).

## Começando

Projeto desenvolvido em Angular 9.1.7, para começar, faça o download ou utilize seu terminal de preferência e clone o respositório com o seguinte comando: `git clone https://dnuneswebdev@bitbucket.org/dnuneswebdev/gimme-pizza.git`

### Instalação

Para rodar o projeto, primeiro instale as dependências pelo comando `npm install`.
Após concluida a instalação, basta rodar o comando `ng serve --open`

### Features

O sistema conta com um cadastro de clientes e pedidos, onde é possível vizualizar, editar, deletar e exportar a lista em PDF e XLS.  
Só é possivel deletar um cliente se ele não tiver pedidos cadastrados.  
É possivel alterar o status de um pedido através da tabela. Clicando sobre o status, irá abrir uma lista de status pré-definidos e, ao escolher um status diferente, será atualizado automaticamente.

## Autor do projeto

Daniel Nunes / dnuneswebdev  
[Linkedin](https://www.linkedin.com/in/danielnsantos/)  
[BitBucket](https://bitbucket.org/dnuneswebdev/)  
[Github](https://github.com/dnuneswebdev)
