export const environment = {
  production: true,
  api: 'https://gimme-pizza-api.herokuapp.com'
};
