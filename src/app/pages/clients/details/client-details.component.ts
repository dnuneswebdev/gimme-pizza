import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ClientsService } from '../services/clients.service';
import { Client } from '../models/client.model';
import { UxService } from 'src/app/shared/services/ux.service';

@Component({
  selector: 'app-client-details',
  templateUrl: './client-details.component.html',
  styleUrls: ['./client-details.component.scss']
})
export class ClientDetailsComponent implements OnInit {

  clientForm: FormGroup;
  isEditing: boolean = false;
  isSubmittingForm: boolean = false;
  isLoading: boolean = false;
  routeParams: any;
  clientId: number;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private clientsService: ClientsService,
    private uxService: UxService,
  ) { }

  ngOnInit(): void {
    this.buildClientForm();

    if (this.checkIfIsEditing()) {
      this.getClientData();
    }
  }

  buildClientForm() {
    this.clientForm = this.formBuilder.group({
      clientContacts: this.formBuilder.group({
        email: ['', [Validators.required, Validators.email]],
        phone: ['', Validators.required]
      }),
      clientAddress: this.formBuilder.group({
        streetAddress: ['', Validators.required],
        number: [null, [Validators.required, Validators.minLength(1)]],
        complement: [''],
        neighborhood: ['', Validators.required],
        city: ['', Validators.required],
        state: ['', Validators.required],
        zipCode: ['', Validators.required]
      }),
      fullName: ['', Validators.required],
      identityId: ['', Validators.required]
    });
  }

  checkIfIsEditing(): boolean {
    this.route.url.subscribe((route) => {
      this.routeParams = route;
    });

    if (this.routeParams[0].path === 'edit') {
      this.clientId = parseInt(this.routeParams[1].path);
      this.isEditing = true;
      return true;
    } else {
      return false;
    }
  }

  getClientData() {
    this.isLoading = true;

    this.clientsService.getSingleClient(this.clientId)
      .subscribe((client) => {
        this.patchValueClientForm(client);
        this.isLoading = false;
      }, error => {
        console.log(error);
        this.isLoading = false;
      })
  }

  patchValueClientForm(client) {
    const clientData: Client = client.data;

    this.clientForm.patchValue({
      id: client.id,
      clientContacts: clientData.clientContacts,
      clientAddress: clientData.clientAddress,
      fullName: clientData.fullName,
      identityId: clientData.identityId
    });

  }

  submitClientForm(clientForm) {
    const client = new Client(
      0,
      clientForm.clientContacts,
      clientForm.clientAddress,
      clientForm.fullName,
      clientForm.identityId
    );

    if (this.isEditing) {
      client.id = this.clientId;
      this.updateClient(client);
    } else {
      this.createClient(client);
    }
  }

  createClient(client: Client) {
    this.isSubmittingForm = true;
    this.isLoading = true;

    this.clientsService.createClient(client)
      .subscribe((response) => {
        this.isLoading = false;
        this.router.navigate(['/clients'])
        this.uxService.success(`Cliente cadastrado com sucesso!`);
      }, error => {
        console.log(error);
        this.isLoading = false;
        this.uxService.error(`Erro! Não foi possivel cadastrar o cliente.`);
      });
  }

  updateClient(client: Client) {
    this.isSubmittingForm = true;
    this.isLoading = true;

    this.clientsService.updateCliente(client, client.id)
      .subscribe((response) => {
        this.isLoading = false;
        this.router.navigate(['/clients'])
        this.uxService.success(`Cliente alterado com sucesso!`);
      }, error => {
        console.log(error);
        this.isLoading = false;
        this.uxService.error(`Erro! Não foi possivel salvar as alterações.`);
      });
  }

  cancelClientForm() {
    this.router.navigate(['/clients']);
    this.clientForm.reset();
  }



}
