import { Component, OnInit, Input, Output, EventEmitter, } from '@angular/core';
import { Client } from '../../models/client.model';

@Component({
  selector: 'app-client-modal',
  templateUrl: './client-modal.component.html',
  styleUrls: ['./client-modal.component.scss']
})
export class ClientModalComponent implements OnInit {

  @Input() client: Client;
  @Input() viewClientModal: boolean;
  @Input() deleteClientModal: boolean;

  @Output() delete = new EventEmitter();
  @Output() closeVModal = new EventEmitter();
  @Output() closeDModal = new EventEmitter();


  constructor() { }

  ngOnInit(): void {

  }

  deleteClient() {
    this.delete.emit(this.client);
  }

  closeViewModal() {
    this.closeVModal.emit(false);
  }

  closeDeleteModal() {
    this.closeDModal.emit(false);
  }

}
