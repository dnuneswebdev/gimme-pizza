import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ClientsRoutingModule } from './clients-routing.module';
import { ClientsComponent } from './main/clients.component';
import { ClientDetailsComponent } from './details/client-details.component';
import { ClientsService } from './services/clients.service';

import { PageTitleModule } from 'src/app/shared/components/page-title/page-title.module';
import { DatatableModule } from 'src/app/shared/components/datatable/datatable.module';
import { ButtonModule } from 'primeng/button';
import { CardModule } from 'primeng/card';
import { ToolbarModule } from 'primeng/toolbar';
import { InputTextModule } from 'primeng/inputtext';
import { InputMaskModule } from 'primeng/inputmask';
import { InputNumberModule } from 'primeng/inputnumber';
import { DialogModule } from 'primeng/dialog';
import { ClientModalComponent } from './components/modal/client-modal.component';
import { LoaderModule } from 'src/app/shared/components/loader/loader.module';
import { ExportDataModule } from 'src/app/shared/components/export-data/export-data.module';

@NgModule({
  declarations: [
    ClientsComponent,
    ClientDetailsComponent,
    ClientModalComponent,
  ],
  imports: [
    CommonModule,
    ClientsRoutingModule,
    PageTitleModule,
    DatatableModule,
    LoaderModule,
    ExportDataModule,
    ButtonModule,
    CardModule,
    ToolbarModule,
    FormsModule,
    ReactiveFormsModule,
    InputTextModule,
    InputMaskModule,
    InputNumberModule,
    DialogModule,
  ],
  providers: [
    ClientsService
  ]
})
export class ClientsModule { }
