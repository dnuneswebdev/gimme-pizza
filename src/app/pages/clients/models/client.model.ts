export class Client {
  id: number;
  clientContacts: ClientContacts;
  clientAddress: ClientAddress;
  fullName: string;
  identityId: string;

  constructor(
    id: number,
    clientContacts: ClientContacts,
    clientAddress: ClientAddress,
    fullName: string,
    indentityId: string
  ) {
    this.id = id;
    this.clientContacts = clientContacts;
    this.clientAddress = clientAddress;
    this.fullName = fullName;
    this.identityId = indentityId;
  }
}

export interface ClientContacts {
  email: string;
  phone: string;
}

export interface ClientAddress {
  streetAddress: string;
  number: string;
  complement: string;
  neighborhood: string;
  city: string;
  state: string;
  zipCode: string;
}