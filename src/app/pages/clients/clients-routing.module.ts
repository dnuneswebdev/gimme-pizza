import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientsComponent } from './main/clients.component';
import { ClientDetailsComponent } from './details/client-details.component';


const routes: Routes = [
  { path: '', component: ClientsComponent },
  { path: 'add', component: ClientDetailsComponent },
  { path: 'edit/:id', component: ClientDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientsRoutingModule { }
