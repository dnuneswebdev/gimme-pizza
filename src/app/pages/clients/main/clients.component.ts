import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { Column, TableApiResponse, TableData } from 'src/app/shared/components/datatable/models/datatable.model';
import { ClientsService } from '../services/clients.service';
import { Client } from '../models/client.model';
import { PizzaOrdersService } from '../../pizza-orders/services/pizza-orders.service';
import { UxService } from 'src/app/shared/services/ux.service';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ClientsComponent implements OnInit {

  columns: Array<Column> = [
    { field: 'fullName', header: 'Nome', hasSort: true, hasFilter: true },
    { field: 'clientContacts', subfield: 'email', header: 'E-mail' },
    { field: 'clientContacts', subfield: 'phone', header: 'Telefone' },
    { field: 'identityId', header: 'CPF' },
    { field: '', header: 'Ações', hasActions: true },
  ]

  tableData: TableData;
  exportTableData: Array<any>;
  viewClientModal: boolean = false;
  deleteClientModal: boolean = false;
  client: Client;

  constructor(
    private router: Router,
    private clientsService: ClientsService,
    private pizzaOrdersService: PizzaOrdersService,
    private uxService: UxService,
  ) { }

  ngOnInit(): void {
    this.getAllClients();
  }

  getAllClients() {
    this.clientsService.getAllClients()
      .subscribe((response: TableApiResponse) => {
        this.tableData = response.data;
        this.buildExportTableData(response.data.content);
      });
  }

  buildExportTableData(clientData) {
    this.exportTableData = [];

    clientData.forEach((client: Client) => {
      const exportData = {
        fullName: client.fullName,
        email: client.clientContacts.email,
        phone: client.clientContacts.phone,
        identityId: client.identityId
      }
      this.exportTableData.push(exportData);
    });
  }

  addClient() {
    this.router.navigate(['/clients/add']);
  }

  viewClient(client) {
    this.client = client;
    this.viewClientModal = true;
  }

  editClient(client) {
    this.router.navigate([`/clients/edit/${client.id}`])
  }

  deleteClientDialog(client) {
    this.client = client;
    this.deleteClientModal = true;
  }

  deleteClient(client: Client) {
    this.deleteClientModal = false;

    if (this.verifyIfClientHasPizzaOrder(client)) {
      this.uxService.error('Erro! O cliente possui pedidos cadastrados.');
    } else {
      this.clientsService.deleteClient(client.id)
        .subscribe((response) => {
          this.uxService.success('Cliente deletado com sucesso!');
          this.getAllClients();
        }, error => {
          console.log(error);
          this.uxService.error(error.errors[0]);
        });
    }

  }

  verifyIfClientHasPizzaOrder(client: Client): boolean {
    const foundClient = this.pizzaOrdersService.pizzaOrdersArray.find((pizzaOrder) => {
      if (client.identityId === pizzaOrder.client.identityId) {
        return true;
      } else {
        return false;
      }
    });

    if (foundClient) {
      return true;
    } else {
      return false;
    }
  }

}
