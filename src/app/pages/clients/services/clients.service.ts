import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Client } from '../models/client.model';

@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  constructor(
    private http: HttpClient,
  ) { }

  getAllClients() {
    return this.http.get(`${environment.api}/clients?page=0&orderBy=fullName`);
  }

  getSingleClient(clientId: number) {
    return this.http.get(`${environment.api}/clients/${clientId}`);
  }

  createClient(client: Client) {
    return this.http.post(`${environment.api}/clients`, client);
  }

  updateCliente(client: Client, clientId: number) {
    return this.http.put(`${environment.api}/clients/${clientId}`, client);
  }

  deleteClient(clientId: number) {
    return this.http.delete(`${environment.api}/clients/${clientId}`);
  }
}
