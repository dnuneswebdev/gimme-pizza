import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { Column, TableData, TableApiResponse } from 'src/app/shared/components/datatable/models/datatable.model';
import { PizzaOrder, OrderStatus } from '../models/pizza-order.model';
import { PizzaOrdersService } from '../services/pizza-orders.service';
import { UxService } from 'src/app/shared/services/ux.service';

@Component({
  selector: 'app-pizza-orders',
  templateUrl: './pizza-orders.component.html',
  styleUrls: ['./pizza-orders.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PizzaOrdersComponent implements OnInit {

  columns: Array<Column> = [
    { field: 'status', hasSort: true, header: 'Status' },
    { field: 'id', hasFilter: true, hasSort: true, header: 'Nº do Pedido' },
    { field: 'client', subfield: 'fullName', header: 'Cliente' },
    { field: 'client', subfield: 'identityId', hasFilter: true, header: 'CPF' },
    { field: '', hasActions: true, header: 'Ações' },
  ]

  orderStatus: Array<OrderStatus> = [
    { label: 'Aberto', value: 'ABERTO' },
    { label: 'Contatar Cliente', value: 'CONTATAR_CLIENTE' },
    { label: 'Preparando', value: 'PREPARANDO' },
    { label: 'Saiu Para Entrega', value: 'SAIU_PARA_ENTREGA' },
    { label: 'Cancelado', value: 'CANCELADO' },
    { label: 'Finalizado', value: 'FINALIZADO' },
  ]

  tableData: TableData;
  exportTableData: Array<any>;
  viewPizzaOrderModal: boolean = false;
  deletePizzaOrderModal: boolean = false;
  isStatusChanged: boolean = false;
  isLoading: boolean = false;
  pizzaOrder: PizzaOrder;

  constructor(
    private router: Router,
    private pizzaOrdersService: PizzaOrdersService,
    private uxService: UxService,
  ) { }

  ngOnInit(): void {
    this.getAllPizzaOrders();
  }

  getAllPizzaOrders() {
    this.pizzaOrdersService.getAllPizzaOrders()
      .subscribe((response: TableApiResponse) => {
        this.tableData = response.data;
        this.buildExportTableData(response.data);
      });
  }

  buildExportTableData(pizzaOrdersData) {
    this.exportTableData = [];

    pizzaOrdersData.forEach((pizzaOrder: PizzaOrder) => {
      const exportData = {
        status: pizzaOrder.status,
        id: pizzaOrder.id,
        fullName: pizzaOrder.client.fullName,
        identityId: pizzaOrder.client.identityId,
      }
      this.exportTableData.push(exportData);
    });
  }

  addPizzaOrder() {
    this.router.navigate(['/pizza-orders/add']);
  }

  viewPizzaOrder(pizzaOrder) {
    this.pizzaOrder = pizzaOrder;
    this.viewPizzaOrderModal = true;
  }

  editPizzaOrder(pizzaOrder) {
    this.router.navigate([`/pizza-orders/edit/${pizzaOrder.id}`])
  }

  deletePizzaOrderDialog(pizzaOrder) {
    this.pizzaOrder = pizzaOrder;
    this.deletePizzaOrderModal = true;
  }

  deletePizzaOrder(pizzaOrder) {
    this.deletePizzaOrderModal = false;
    this.isLoading = true;

    this.pizzaOrdersService.deletePizzaOrder(pizzaOrder.id)
      .subscribe((response) => {
        this.uxService.success('Pedido deletado com sucesso!');
        this.isLoading = false;
        this.getAllPizzaOrders();
      }, error => {
        console.log(error);
        this.isLoading = false;
      });
  }

  changePizzaOrderStatus(pizzaOrder: PizzaOrder) {
    this.isStatusChanged = true;

    this.pizzaOrdersService.updatePizzaOrder(pizzaOrder, pizzaOrder.id)
      .subscribe((response) => {
        this.isStatusChanged = false;
        this.uxService.success('Status do pedido alterado com sucesso!');
        this.getAllPizzaOrders();
      }, error => {
        console.log(error);
        this.isStatusChanged = false;
        this.uxService.error('Erro! Não foi possivel alterar o status do pedido.');
      });
  }

}
