import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PizzaOrder } from '../../models/pizza-order.model';

@Component({
  selector: 'app-pizza-orders-modal',
  templateUrl: './pizza-orders-modal.component.html',
  styleUrls: ['./pizza-orders-modal.component.scss']
})
export class PizzaOrdersModalComponent implements OnInit {

  @Input() pizzaOrder: PizzaOrder;
  @Input() viewPizzaOrderModal: boolean;
  @Input() deletePizzaOrderModal: boolean;

  @Output() delete = new EventEmitter();
  @Output() closeVModal = new EventEmitter();
  @Output() closeDModal = new EventEmitter();


  constructor() { }

  ngOnInit(): void {

  }

  deletePizzaOrder() {
    this.delete.emit(this.pizzaOrder);
  }

  closeViewModal() {
    this.closeVModal.emit(false);
  }

  closeDeleteModal() {
    this.closeDModal.emit(false);
  }

}
