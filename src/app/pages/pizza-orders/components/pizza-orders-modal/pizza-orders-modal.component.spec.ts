import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PizzaOrdersModalComponent } from './pizza-orders-modal.component';

describe('PizzaOrdersModalComponent', () => {
  let component: PizzaOrdersModalComponent;
  let fixture: ComponentFixture<PizzaOrdersModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PizzaOrdersModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PizzaOrdersModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
