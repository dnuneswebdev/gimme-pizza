import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PizzaOrdersService } from '../services/pizza-orders.service';
import { ClientsService } from '../../clients/services/clients.service';
import { TableApiResponse } from 'src/app/shared/components/datatable/models/datatable.model';
import { Client } from '../../clients/models/client.model';
import { PizzaOrder } from '../models/pizza-order.model';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { UxService } from 'src/app/shared/services/ux.service';

@Component({
  selector: 'app-pizza-order-details',
  templateUrl: './pizza-order-details.component.html',
  styleUrls: ['./pizza-order-details.component.scss']
})
export class PizzaOrderDetailsComponent implements OnInit {

  flavors: Array<any> = [
    { label: 'Marguerita', value: 'MARGUERITA' },
    { label: 'Calabresa', value: 'CALABRESA' },
    { label: 'Pepperoni', value: 'PEPPERONI' },
    { label: 'Portugues', value: 'PORTUGUES' },
    { label: 'Quatro Queijos', value: 'QUATRO_QUEIJOS' },
  ];

  sizes: Array<any> = [
    { label: 'P', value: 'P' },
    { label: 'M', value: 'M' },
    { label: 'G', value: 'G' },
  ];

  pizzaOrderForm: FormGroup;
  tabIndex: number = 0;
  isEditing: boolean = false;
  isLoading: boolean = false;
  isSameAddress: boolean = false;
  routeParams: any;
  selectedCrust: string = 'FINA';
  calendarDate: Date;
  timeDate: Date;
  nowDate: Date = new Date(Date.now());
  clientIdentityId: string = '';
  pizzaOrderId: number;
  client: Client;

  constructor(
    private formBuilder: FormBuilder,
    private pizzaOrdersService: PizzaOrdersService,
    private clientesService: ClientsService,
    private datePipe: DatePipe,
    private route: ActivatedRoute,
    private router: Router,
    private uxService: UxService,
  ) { }

  ngOnInit(): void {
    this.buildPizzaOrderForm();

    if (this.checkIfIsEditing()) {
      this.getPizzaOrderData();
    }
  }

  buildPizzaOrderForm() {
    this.pizzaOrderForm = this.formBuilder.group({
      client: this.formBuilder.group({
        clientContacts: this.formBuilder.group({
          email: ['', [Validators.required, Validators.email]],
          phone: ['', Validators.required]
        }),
        clientAddress: this.formBuilder.group({
          streetAddress: ['', Validators.required],
          number: [null, [Validators.required, Validators.minLength(1)]],
          complement: [''],
          neighborhood: ['', Validators.required],
          city: ['', Validators.required],
          state: ['', Validators.required],
          zipCode: ['', Validators.required]
        }),
        fullName: ['', Validators.required],
        identityId: ['', Validators.required]
      }),
      pizzas: this.formBuilder.array([
        this.formBuilder.group({
          halfNHalf: [false],
          extraSauce: [false],
          crust: [''],
          mainFlavor: [null, [Validators.required]],
          secondaryFlavor: [null],
          size: [null, [Validators.required]],
          note: ['']
        })
      ]),
      deliveryAddress: this.formBuilder.group({
        streetAddress: ['', Validators.required],
        number: [null, [Validators.required, Validators.minLength(1)]],
        complement: [''],
        neighborhood: ['', Validators.required],
        city: ['', Validators.required],
        state: ['', Validators.required],
        zipCode: ['', Validators.required]
      }),
      scheduled: [false],
      scheduledDate: [''],
      scheduledTime: ['']
    });
  }

  checkIfIsEditing(): boolean {
    this.route.url.subscribe((route) => {
      this.routeParams = route;
    });

    if (this.routeParams[0].path === 'edit') {
      this.pizzaOrderId = parseInt(this.routeParams[1].path);
      this.isEditing = true;
      return true;
    } else {
      return false;
    }
  }

  getPizzaOrderData() {
    this.isLoading = true;

    this.pizzaOrdersService.getSinglePizzaOrder(this.pizzaOrderId)
      .subscribe((pizzaOrder) => {
        this.patchValuePizzaOrderForm(pizzaOrder);
        this.isLoading = false;
      }, error => {
        console.log(error);
        this.isLoading = false;
      })
  }

  patchValuePizzaOrderForm(pizzaOrder) {
    const pizzaOrderData: PizzaOrder = pizzaOrder.data;
    this.client = pizzaOrder.data.client;

    this.pizzaOrderForm.patchValue({
      id: pizzaOrderData.id,
      client: pizzaOrderData.client,
      pizzas: pizzaOrderData.pizzas,
      deliveryAddress: pizzaOrderData.deliveryAddress,
      scheduled: pizzaOrderData.scheduled,
      scheduledDate: pizzaOrderData.scheduledDate,
      scheduledTime: pizzaOrderData.scheduledTime,
      status: pizzaOrderData.status
    });
  }


  submitPizzaOrderForm(pizzaOrderValue) {
    const pizzaOrder = new PizzaOrder(
      0,
      pizzaOrderValue.client,
      pizzaOrderValue.pizzas,
      pizzaOrderValue.deliveryAddress,
      pizzaOrderValue.scheduled,
      pizzaOrderValue.scheduledDate,
      pizzaOrderValue.scheduledTime,
      'ABERTO'
    );

    if (!pizzaOrder.client.id && !this.client) {
      pizzaOrder.client.id = 0;
    } else {
      pizzaOrder.client.id = this.client.id;
    }

    if (pizzaOrder.scheduled === false) {
      pizzaOrder.scheduledDate = null;
      pizzaOrder.scheduledTime = null;
    }

    pizzaOrder.scheduledDate = this.datePipe.transform(pizzaOrder.scheduledDate, 'yyyy-MM-dd');
    pizzaOrder.scheduledTime = this.datePipe.transform(pizzaOrder.scheduledTime, 'HH:mm:ss');

    if (this.isEditing) {
      pizzaOrder.id = this.pizzaOrderId;
      this.updatePizzaOrder(pizzaOrder);
    } else {
      this.createPizzaOrder(pizzaOrder);
    }

  }

  createPizzaOrder(pizzaOrder) {
    this.isLoading = true;

    this.pizzaOrdersService.createPizzaOrder(pizzaOrder)
      .subscribe((response) => {
        this.isLoading = false;
        this.router.navigate(['/pizza-orders']);
        this.uxService.success('Pedido criado com sucesso!');
      }, error => {
        console.log(error);
        this.isLoading = false;
        this.uxService.error('Não foi possivel criar o pedido!');
      });
  }

  updatePizzaOrder(pizzaOrder) {
    this.isLoading = true;

    this.pizzaOrdersService.updatePizzaOrder(pizzaOrder, pizzaOrder.id)
      .subscribe((response) => {
        this.isLoading = false;
        this.router.navigate(['/pizza-orders'])
        this.uxService.success(`Pedido alterado com sucesso!`);
      }, error => {
        console.log(error);
        this.isLoading = false;
        this.uxService.error(`Erro! Não foi possivel salvar as alterações.`);
      });
  }

  searchClient() {
    this.isLoading = true;
    this.clientesService.getAllClients()
      .subscribe((response: TableApiResponse) => {
        response.data.content.filter((client: Client) => {
          if (client.identityId === this.clientIdentityId) {
            this.client = client;
            this.patchValueClientForm();
            this.isLoading = false;
          }
        });
      }, error => {
        console.log(error);
        this.isLoading = false;
      })
  }

  patchValueClientForm() {
    this.pizzaOrderForm.patchValue({
      client: {
        id: this.client.id,
        clientContacts: this.client.clientContacts,
        clientAddress: this.client.clientAddress,
        fullName: this.client.fullName,
        identityId: this.client.identityId
      }
    });
  }

  patchValueDeliveryAddressForm() {
    if (this.isSameAddress) {
      this.pizzaOrderForm.patchValue({
        deliveryAddress: this.client.clientAddress
      });
    } else {
      this.pizzaOrderForm.controls.deliveryAddress.reset();
    }
  }

  cancelPizzaOrder() {
    this.router.navigate(['/pizza-orders']);
    this.pizzaOrderForm.reset();
  }

  prevTab() {
    this.tabIndex = (this.tabIndex === 0) ? 2 : this.tabIndex - 1;
  }

  nextTab() {
    this.tabIndex = (this.tabIndex === 2) ? 0 : this.tabIndex + 1;
  }

}
