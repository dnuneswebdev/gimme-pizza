import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PizzaOrdersComponent } from './main/pizza-orders.component';
import { PizzaOrderDetailsComponent } from './details/pizza-order-details.component';


const routes: Routes = [
  { path: '', component: PizzaOrdersComponent },
  { path: 'add', component: PizzaOrderDetailsComponent },
  { path: 'edit/:id', component: PizzaOrderDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PizzaOrdersRoutingModule { }
