import { Client } from '../../clients/models/client.model';

export class PizzaOrder {
  id: number;
  client: Client;
  pizzas: Array<Pizza>;
  deliveryAddress: DeliveryAddress;
  scheduled: boolean;
  scheduledDate: Date | string;
  scheduledTime: string;
  status: string;

  constructor(
    id: number,
    client: Client,
    pizzas: Array<Pizza>,
    deliveryAddress: DeliveryAddress,
    scheduled: boolean,
    scheduledDate: Date | string,
    scheduledTime: string,
    status: string,
  ) {
    this.id = id;
    this.client = client;
    this.pizzas = pizzas;
    this.deliveryAddress = deliveryAddress;
    this.scheduled = scheduled;
    this.scheduledDate = scheduledDate;
    this.scheduledTime = scheduledTime;
    this.status = status;
  }
}

export interface Pizza {
  halfNHalf: boolean;
  extraSauce: boolean;
  crust: string;
  mainFlavor: string;
  secondaryFlavor: string;
  size: string;
  note: string;
}

export interface DeliveryAddress {
  streetAddress: string;
  number: string;
  complement: string;
  neighborhood: string;
  city: string;
  state: string;
  zipCode: string;
}

export interface OrderStatus {
  label: string;
  value: string;
}