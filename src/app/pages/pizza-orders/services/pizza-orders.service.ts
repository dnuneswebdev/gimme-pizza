import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { PizzaOrder } from '../models/pizza-order.model';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PizzaOrdersService {

  pizzaOrdersArray: Array<PizzaOrder> = [];

  constructor(
    private http: HttpClient,
  ) { }

  getAllPizzaOrders() {
    return this.http.get(`${environment.api}/pizza-orders?page=0&orderBy=status`)
      .pipe(tap((pizzaOrders: any) => {
        this.pizzaOrdersArray = pizzaOrders.data;
      }))
  }

  getSinglePizzaOrder(pizzaOrderId: number) {
    return this.http.get(`${environment.api}/pizza-orders/${pizzaOrderId}`);
  }

  createPizzaOrder(pizzaOrder: PizzaOrder) {
    return this.http.post(`${environment.api}/pizza-orders`, pizzaOrder);
  }

  updatePizzaOrder(pizzaOrder: PizzaOrder, pizzaOrderId: number) {
    return this.http.put(`${environment.api}/pizza-orders/${pizzaOrderId}`, pizzaOrder);
  }

  deletePizzaOrder(pizzaOrderId: number) {
    return this.http.delete(`${environment.api}/pizza-orders/${pizzaOrderId}`);
  }

  uploadPizzaOrder(pizzaOrderFile) {
    return this.http.post(`${environment.api}/pizza-orders/upload`, pizzaOrderFile);
  }
}
