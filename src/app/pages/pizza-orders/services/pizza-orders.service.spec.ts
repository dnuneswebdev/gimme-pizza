import { TestBed } from '@angular/core/testing';

import { PizzaOrdersService } from './pizza-orders.service';

describe('PizzaOrdersService', () => {
  let service: PizzaOrdersService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PizzaOrdersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
