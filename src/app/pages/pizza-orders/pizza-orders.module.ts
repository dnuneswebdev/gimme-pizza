import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PizzaOrdersRoutingModule } from './pizza-orders-routing.module';
import { PizzaOrdersComponent } from './main/pizza-orders.component';
import { PizzaOrderDetailsComponent } from './details/pizza-order-details.component';
import { PizzaOrdersModalComponent } from './components/pizza-orders-modal/pizza-orders-modal.component';
import { PageTitleModule } from 'src/app/shared/components/page-title/page-title.module';
import { DatatableModule } from 'src/app/shared/components/datatable/datatable.module';
import { LoaderModule } from 'src/app/shared/components/loader/loader.module';

import { ButtonModule } from 'primeng/button';
import { CardModule } from 'primeng/card';
import { ToolbarModule } from 'primeng/toolbar';
import { InputTextModule } from 'primeng/inputtext';
import { InputMaskModule } from 'primeng/inputmask';
import { InputNumberModule } from 'primeng/inputnumber';
import { DialogModule } from 'primeng/dialog';
import { InputSwitchModule } from 'primeng/inputswitch';
import { CalendarModule } from 'primeng/calendar';
import { DropdownModule } from 'primeng/dropdown';
import { TabViewModule } from 'primeng/tabview';
import { RadioButtonModule } from 'primeng/radiobutton';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ExportDataModule } from 'src/app/shared/components/export-data/export-data.module';
import { ImportDataModule } from 'src/app/shared/components/import-data/import-data.module';

@NgModule({
  declarations: [
    PizzaOrdersComponent,
    PizzaOrderDetailsComponent,
    PizzaOrdersModalComponent,
  ],
  imports: [
    CommonModule,
    PizzaOrdersRoutingModule,
    PageTitleModule,
    DatatableModule,
    LoaderModule,
    ExportDataModule,
    ImportDataModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    CardModule,
    ToolbarModule,
    InputTextModule,
    InputMaskModule,
    InputNumberModule,
    DialogModule,
    InputSwitchModule,
    CalendarModule,
    DropdownModule,
    TabViewModule,
    RadioButtonModule,
    InputTextareaModule,
  ],
  providers: [
    DatePipe
  ]
})
export class PizzaOrdersModule { }
