import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Login, Token } from '../models/auth.model';
import { environment } from 'src/environments/environment';
import { tap } from 'rxjs/operators';
import { AuthStoreService } from 'src/app/stores/auth/auth-store.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
    private authStore: AuthStoreService,
  ) { }

  login(userLogin: Login, basicAuth: string) {
    return this.http.post(`${environment.api}/authentication`,
      userLogin,
      { headers: { 'Authorization': `Basic ${basicAuth}` } })
      .pipe(tap((token: Token) => {
        this.authStore.token = token.token;
      }));
  }

}
