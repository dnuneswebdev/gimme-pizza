import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthStoreService } from 'src/app/stores/auth/auth-store.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private authStore: AuthStoreService
  ) { }

  canActivate(): boolean {
    if (this.authStore.isAuth === false) {
      this.router.navigate(['/login'])
      return false;
    }
    return true;
  }

}
