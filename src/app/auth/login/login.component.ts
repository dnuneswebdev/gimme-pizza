import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Login } from '../models/auth.model';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { AuthStoreService } from 'src/app/stores/auth/auth-store.service';
import { UxService } from 'src/app/shared/services/ux.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {

  username: string = 'frezze';
  password: string = 'fr3ZZ&l0g'

  loginForm: FormGroup;
  isSubmittingLoginForm: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private authStore: AuthStoreService,
    private uxService: UxService,
  ) { }

  ngOnInit(): void {
    this.authStore.isAuth = false;
    this.createLoginForm();
  }

  createLoginForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  submitLoginForm(login: Login) {
    this.isSubmittingLoginForm = true;

    const basicAuth = btoa(`${login.username}:${login.password}`);

    this.authService.login(login, basicAuth)
      .subscribe((response: any) => {
        this.authStore.isAuth = true;
        this.router.navigate(['/home']);
        this.isSubmittingLoginForm = false;
        this.uxService.success(`Logado com sucesso! Bem-vindo, ${login.username}.`);
      }, error => {
        console.log(error);
        this.uxService.error(`Erro! Não foi possivel acessar o sistema.`);
        this.isSubmittingLoginForm = false;
      });
  }

}
