import { BehaviorSubject, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { Token } from 'src/app/auth/models/auth.model';

@Injectable({ providedIn: 'root' })
export class AuthStoreService {

  private readonly _isLoggedIn = new BehaviorSubject<boolean>(false);
  readonly isLoggedIn = this._isLoggedIn.asObservable();

  private readonly _userToken = new BehaviorSubject<string>('');
  readonly userToken = this._userToken.asObservable();

  get isAuth(): boolean {
    return this._isLoggedIn.getValue();
  }

  set isAuth(value: boolean) {
    this._isLoggedIn.next(value);
  }

  get token(): string {
    return this._userToken.getValue();
  }

  set token(value: string) {
    this._userToken.next(value);
  }

}