export interface Column {
  field: string;
  subfield?: string;
  header: string;
  hasFilter?: boolean;
  hasSort?: boolean;
  hasActions?: boolean;
}

export interface TableData {
  content: Array<any>;
  empty: boolean;
  first: boolean;
  last: boolean;
  number: number;
  numberOfElements: number;
  pageable: Pageable;
  size: number;
  sort: Sort;
  totalElements: number;
  totalPages: number;
}

export interface Pageable {
  offset: number;
  pageNumber: number;
  pageSize: number;
  paged: boolean;
  sort: Sort;
  unpaged: boolean;
}

export interface Sort {
  empty: boolean;
  sorted: boolean;
  unsorted: boolean;
}

export interface TableApiResponse {
  data: TableData;
  errors: Array<any>;
  message: string;
  status: number;
}

