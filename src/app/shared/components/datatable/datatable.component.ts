import { Component, OnInit, Input, SimpleChanges, OnChanges, Output, EventEmitter } from '@angular/core';
import { Column, TableData } from './models/datatable.model';
import { OrderStatus } from 'src/app/pages/pizza-orders/models/pizza-order.model';

@Component({
  selector: 'app-datatable',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.scss']
})
export class DatatableComponent implements OnInit, OnChanges {

  @Input() columns: Array<Column>;
  @Input() orderStatus: Array<OrderStatus>;
  @Input() tableData: any;
  @Input() isStatusChanged: boolean = false;

  @Output() viewItem = new EventEmitter();
  @Output() editItem = new EventEmitter();
  @Output() deleteItem = new EventEmitter();
  @Output() statusItem = new EventEmitter();

  totalRecords: number;
  page: number;
  pageSize: number;
  data: Array<any>;
  filteredData: Array<any>;
  isLoading: boolean = true;

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.tableData) {

      if (!this.tableData.totalElements) {
        this.totalRecords = this.tableData.length;
      } else {
        this.totalRecords = this.tableData.totalElements;
      }

      if (!this.tableData.content) {
        this.data = this.tableData;
        this.filteredData = [...this.data];
      } else {
        this.data = this.tableData.content;
        this.filteredData = [...this.data];
      }

      this.isLoading = false;
    }
  }

  viewTableItem(item) {
    this.viewItem.emit(item);
  }

  editTableItem(item) {
    this.editItem.emit(item)
  }

  deleteTableItem(item) {
    this.deleteItem.emit(item);
  }

  changeTableItemStatus(item) {
    this.statusItem.emit(item);
  }

  filterSubfield(value: string, field: string, subfield: string) {
    this.filteredData = this.data.filter((item) => {
      return item[field][subfield].toLocaleLowerCase().includes(value.trim().toLocaleLowerCase());
    });
  }

}
