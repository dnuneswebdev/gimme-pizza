import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImportDataComponent } from './import-data.component';
import { FileUploadModule } from 'primeng/fileupload';
import { ButtonModule } from 'primeng/button';


@NgModule({
  declarations: [ImportDataComponent],
  imports: [
    CommonModule,
    ButtonModule,
    FileUploadModule
  ],
  exports: [
    ImportDataComponent
  ]
})
export class ImportDataModule { }
