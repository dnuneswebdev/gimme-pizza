import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { PizzaOrdersService } from 'src/app/pages/pizza-orders/services/pizza-orders.service';
import { UxService } from '../../services/ux.service';

@Component({
  selector: 'app-import-data',
  templateUrl: './import-data.component.html',
  styleUrls: ['./import-data.component.scss']
})
export class ImportDataComponent implements OnInit {

  @Output() isLoading = new EventEmitter();

  constructor(
    private pizzaOrdersService: PizzaOrdersService,
    private uxService: UxService,
  ) { }

  ngOnInit(): void {
  }

  completedUploadFile(fileEvent) {
    this.isLoading.emit(false);
    this.uxService.success('Pedido cadastrado com sucesso!');
  }

  beforeUploadingFile(event) {
    this.isLoading.emit(true);
  }

  uploadFailed(event) {
    this.uxService.error('Não foi possível fazer o upload do arquivo.');
  }

}
