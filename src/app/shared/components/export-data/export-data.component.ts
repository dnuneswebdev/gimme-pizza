import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Column } from '../datatable/models/datatable.model';
import { MenuItem } from 'primeng/api';

declare var jsPDF: any;

@Component({
  selector: 'app-export-data',
  templateUrl: './export-data.component.html',
  styleUrls: ['./export-data.component.scss']
})
export class ExportDataComponent implements OnInit, OnChanges {

  @Input() tableData: any;
  @Input() columns: Array<Column>;
  @Input() docName: string;

  exportColumns: Array<any>;

  menuItems: Array<MenuItem> = [
    {
      label: 'PDF',
      icon: 'pi pi-file-pdf',
      command: () => {
        this.exportToPDf();
      }
    },
    {
      label: 'XLS',
      icon: 'pi pi-file-excel',
      command: () => {
        this.exportToExcel();
      }
    }
  ];

  constructor() { }

  ngOnInit(): void { }

  ngOnChanges() {
    this.exportColumns = this.columns.map(column => {
      if (column.subfield) {
        return { title: column.header, dataKey: column.subfield }
      } else {
        return { title: column.header, dataKey: column.field }
      }
    });

    this.exportColumns.forEach((column) => {
      if (column.title === 'Ações') {
        return this.exportColumns.pop();
      }
    })
  }

  exportToPDf() {
    const doc = new jsPDF('l', 'pt');
    doc.autoTable(this.exportColumns, this.tableData);
    doc.save(`${this.docName}.pdf`);
  }

  exportToExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet(this.tableData);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.docName);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }

}
