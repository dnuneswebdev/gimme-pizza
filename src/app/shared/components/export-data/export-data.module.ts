import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExportDataComponent } from './export-data.component';
import { ButtonModule } from 'primeng/button';
import { MenuModule } from 'primeng/menu';


@NgModule({
  declarations: [ExportDataComponent],
  imports: [
    CommonModule,
    ButtonModule,
    MenuModule,
  ],
  exports: [
    ExportDataComponent
  ]
})
export class ExportDataModule { }
