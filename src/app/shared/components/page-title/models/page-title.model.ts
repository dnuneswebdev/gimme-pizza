export interface PageTitle {
  icon: string;
  title: string;
}