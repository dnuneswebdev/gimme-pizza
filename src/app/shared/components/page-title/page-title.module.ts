import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageTitleComponent } from './page-title.component';
import { CardModule } from 'primeng/card';


@NgModule({
  declarations: [
    PageTitleComponent
  ],
  imports: [
    CommonModule,
    CardModule
  ],
  exports: [
    PageTitleComponent
  ]
})
export class PageTitleModule { }
