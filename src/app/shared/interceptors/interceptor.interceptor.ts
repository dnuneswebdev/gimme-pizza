import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { AuthStoreService } from 'src/app/stores/auth/auth-store.service';
import { catchError } from 'rxjs/internal/operators/catchError';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private authStore: AuthStoreService,
  ) { }

  intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    if (this.authStore.isAuth) {
      req = req.clone({
        setHeaders: {
          Authorization: `Bearer ${this.authStore.token}`
        }
      }), catchError((err) => {
        console.log(err);
        if (err instanceof HttpErrorResponse) {
          req = req.clone({ setHeaders: { 'Authorization': `Bearer ${this.authStore.token}` } });
          return next.handle(req);
        }
        return throwError(err);
      });
      return next.handle(req);
    } else {
      return next.handle(req);
    }
  }
}
