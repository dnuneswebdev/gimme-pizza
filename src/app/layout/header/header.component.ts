import { Component, OnInit } from '@angular/core';
import { AuthStoreService } from 'src/app/stores/auth/auth-store.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  isLoggedIn: boolean;

  constructor(
    private authStore: AuthStoreService
  ) { }

  ngOnInit(): void {
    this.userIsLoggedIn();
  }

  userIsLoggedIn() {
    this.authStore.isLoggedIn.subscribe((value) => this.isLoggedIn = value);
  }

}
